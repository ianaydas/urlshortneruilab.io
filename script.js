document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("generateBtn").addEventListener("click", function(event) {
        event.preventDefault();
        var url = document.getElementById("urlInput").value;
        if (url.trim() === '') {
            //document.getElementById("generateBtn").disabled = true;
        } else {
            document.getElementById("generateBtn").disabled = false;
            generateTinyURL(url);
        }

    });

    document.getElementById("copyBtn").addEventListener("click", function() {
        copyToClipboard(document.getElementById("tinyURLInput").value);
    });

    document.getElementById("createNewBtn").addEventListener("click", function() {
        console.log("called...")
        document.getElementById("tinyURLInput").value="";
        document.getElementById("urlInput").value = ""; // Clear the value of urlInput
        document.getElementById("copyBtn").style.display = "block";
        document.getElementById("copyBtnPressed").style.display = "none";
        document.getElementById('qrCodeCanvas').style.display = "none";
    });
});


function generateTinyURL(url) {
    showLoader();
    console.log("valid url");
    console.log(url);
    var formdata = new FormData();
    formdata.append("long_url", url);

    var requestOptions = {
    method: 'POST',
    body: formdata,
    redirect: 'follow'
    };

    fetch("https://52bb-2405-201-9007-908-c969-85d6-bbe2-e815.ngrok-free.app/urlshortner/url/generate-short-url", requestOptions)
    .then(response => response.json())
    .then(data => {
        console.log(data);
        if (data.status === 'SUCCESS') {
            console.log("data:  "+data.data);
            document.getElementById("tinyURLInput").value = data.data;
            //document.getElementById("tinyURLInput").style.display = "block";
            document.getElementById("copyBtn").style.display = "block";
            document.getElementById("copyBtnPressed").style.display = "none";
            document.getElementById("tinyURLContainer").style.display = "block";
            generateQRCode(data.data);
            hideLoader(); 
        } else {
            console.error('Error:', data.error);
            document.getElementById("tinyURLInput").value = `Error: ${data.error}`;
            document.getElementById("tinyURLContainer").style.display = "block";
            hideLoader();
        }
    })
    .catch(error => {
        console.error('Error:', error);
        document.getElementById("tinyURLInput").value = `Error: ${error.message}`;
        document.getElementById("tinyURLContainer").style.display = "block";
        hideLoader();
    });
}

function copyToClipboard(text) {
    var textarea = document.createElement("textarea");
    textarea.value = text;
    document.body.appendChild(textarea);
    textarea.select();
    document.execCommand("copy");
    document.body.removeChild(textarea);
     document.getElementById("copyBtn").style.display = "none";
    document.getElementById("copyBtnPressed").style.display = "block";
    //alert("URL copied to clipboard!");
}

function showLoader() {
    document.getElementById("loader").style.display = "block";
    var loaderContainer = document.getElementById("loader");
    var animation = bodymovin.loadAnimation({
        container: loaderContainer,
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: 'https://lottie.host/8dd29e45-2c79-401f-8dae-4a6aafd49427/KLN145MH9p.json' // Path to your Lottie JSON file
    });
}

function hideLoader() {
    document.getElementById("loader").style.display = "none";
    var loaderContainer = document.getElementById("loader");
    loaderContainer.innerHTML = ""; // Clear the loader container
}

function generateQRCode(url) {

        // Get the canvas element
        document.getElementById('qrCodeCanvas').style.display = "block";
        var canvas = document.getElementById('qrCodeCanvas');

        // Create a QRious instance with options
        var qr = new QRious({
            element: canvas,
            value: url,
            size: 200, // Adjust size as needed
            foreground: '#BF8440', // Color of the QR code squares
            background: '#FFFFFF'
        });

}
    


