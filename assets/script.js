document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("urlForm").addEventListener("submit", function(event) {
        event.preventDefault();
        var url = document.getElementById("urlInput").value;
        generateTinyURL(url);
    });

    document.getElementById("copyB").addEventListener("click", function() {
        copyToClipboard(document.getElementById("tinyURL").innerText);
    });
});


function generateTinyURL(url) {

    var formdata = new FormData();
    formdata.append("long_url", url);

    var requestOptions = {
    method: 'POST',
    body: formdata,
    redirect: 'follow'
    };

    fetch("https://03d1-2405-201-9007-908-c969-85d6-bbe2-e815.ngrok-free.app", requestOptions)
    .then(response => response.json())
    .then(data => {
        console.log(data);
        if (data.status === 'SUCCESS') {
            console.log("data:  "+data.data);
            document.getElementById("tinyURLContainer").innerHTML = `TinyURL: <p id="tinyURL"> <a href="${data.data}" target="_blank">${data.data}</a></p>`;
            document.getElementById("tinyURLContainer").style.display = "block";
            document.getElementById("copyB").style.display = "block"; 
        } else {
            
            console.error('Error:', data.error);
            document.getElementById("tinyURLContainer").innerHTML = `<p>Error: ${data.error}</p>`;
        }
    })
    .catch(error => {
        console.error('Error:', error);
        document.getElementById("tinyURLContainer").innerHTML = `<p>Error: ${error.message}</p>`;
    });
}

function copyToClipboard(text) {
    var textarea = document.createElement("textarea");
    textarea.value = text;
    document.body.appendChild(textarea);
    textarea.select();
    document.execCommand("copy");
    document.body.removeChild(textarea);
    alert("URL copied to clipboard!");
}